document.addEventListener('DOMContentLoaded', () => {
    let tabs = document.querySelectorAll('.partitura_tabs-links a');


    tabs.forEach(function(tab_link)
        {
            tab_link.addEventListener('click', function(e)
                {
                    e.stopPropagation();
                    e.preventDefault();
                    tabs.forEach(function(t)
                        {
                            t.classList.remove('selected');
                        });
                    tab_link.classList.add('selected');
                    let iframe = document.querySelector('.partitura_iframe iframe');
                    let src = iframe.getAttribute('data-src');
                    iframe.setAttribute('src',pdf_viewer+tab_link.getAttribute('href'));


                    return false;
                }, true);
        });
    //window.setTimeout(function() { document.querySelector('.partitura_tabs-links a.selected').click();}, 100);
});
