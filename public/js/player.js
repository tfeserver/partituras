var player = function()
{
    this.mix= [];
    this.offset=0;
    this.buffers= [];
    this.context;
    this.speed =  1;
    this.playing = false;

    this.mixDown= function(bufferList, totalLength, numberOfChannels = 2, deltas = []){
        let finalMix = this.context.createBuffer(numberOfChannels, totalLength, bufferList[0].sampleRate);
        var max = 0;
        for(let i = 0; i < bufferList.length; i++){
            for(let channel = 0; channel < numberOfChannels; channel++){
                let buffer = finalMix.getChannelData(channel);
                for(let j = 0; j < bufferList[i].length; j++){
                    buffer[j] += bufferList[i].getChannelData(channel)[j] * parseFloat(deltas[i] / deltas.length);
                    if(buffer[j]>max) { max=buffer[j]; }
                }
            }
        }
        return finalMix;
    };
    this.load= function(urls, volumevalues, load_callback, mix_callback) {
        this.volumevalues = volumevalues;
        console.log('loading ',urls);
        // Fix up prefixing
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        this.context = new AudioContext();
        this.volumes = [];
        for(var i=0; i<volumevalues.length; i++)
        {
            let volume = this.context.createGain();
            volume.gain.value= volumevalues[i];
            this.volumes.push(volume);
            volume.connect(this.context.destination);
        }

        var bufferLoader = new BufferLoader(this.context, urls , this.finishedLoading.bind(this));
        bufferLoader.load();

        this.loaded_callback = load_callback;
        this.mix_callback = mix_callback;
    };
    this.finishedLoading= function(bufferList) {
        if(!bufferList)
        {
            return alert('An error occured loading the audio.');
        }
        this.buffers = bufferList;
        if(this.loaded_callback)
        {
            this.loaded_callback();
        }
    };

    this.updatemixedbuffer = function()
    {
        this.songLength = 0;
        for(let track of this.buffers){
            if(track.length > this.songLength){
                this.songLength = track.length;
            }
        }
        //call our function here
        this.mixed_buffer = this.mixDown(this.buffers, this.songLength, 2, this.volumes);
    };
    this.setSpeed=   function(speed)
    {
        this.speed = speed;
    }


    this.setVolumes = function(volumevalues)
    {
        for(let i=0; i<volumevalues.length; i++)
        {
            this.volumes[i].gain.setValueAtTime(volumevalues[i], 0);
        }
    }

    this.secToDuration = function(sec)
    {
        let min = 0;
        while(sec>=60)
        {
            min++;
            sec-=60;
        }
        return min+':'+(sec<10 ? '0'+parseInt(sec): parseInt(sec));
    }
    this.getDuration = function()
    {
        return this.secToDuration(this.buffers[0].duration);
    }
    this.getCurrentTime = function()
    {
        if(this.playing)
        {
            return this.secToDuration(this.context.currentTime - this.startedAt);
        }
        else
        {
            return this.secToDuration(this.offset);
        }
    }

    this.play= function(offset)
    {
        let playOffset = this.context.currentTime;
        for(let i=0; i<this.buffers.length; i++)
        {
            var mix = this.context.createBufferSource();
            this.mix.push(mix);
            if(!offset)
            {
                offset=0;
            }

            this.startedAt = this.context.currentTime - offset;
            mix.buffer = this.buffers[i];
            mix.playbackRate.value = this.speed;
            mix.mozPreservesPitch = true;
            mix.preservesPitch = true;
            mix.webkitPreservesPitch = true;
            mix.connect(this.volumes[i]);
            mix.start(playOffset,offset);

        }
        this.playing=true;
    },

    this.pause= function()
    {
        if(this.playing)
        {
            this.offset = this.context.currentTime - this.startedAt;
            this.stop();
        }
        else
        {
            this.play(this.offset);
        }

    }
    this.stop= function()
    {
        this.mix.forEach((mix) =>
        {
            mix.stop();
            mix.disconnect();
        });
        this.mix = [];
        this.playing=false;
    }

    this.prev= function(secs)
    {
        var was_playing = this.playing;
        var resume = null;
        if(was_playing)
        {
            this.pause();
            this.offset -= secs;
            this.offset = Math.max(0, this.offset);
        }
        if(was_playing)
        {
            this.play(this.offset);
        }
    }
    this.next= function(secs)
    {
        var was_playing = this.playing;
        var resume = null;
        if(was_playing)
        {
            this.pause();
            this.offset += secs;
            this.offset = Math.max(0, this.offset);
        }
        if(was_playing)
        {
            this.play(this.offset);
        }
    }
};





