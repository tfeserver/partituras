let audios = document.querySelectorAll('audio');

let loading = true;

var paused = true;

let speed = 100;
let promisesAudio = null;

function loadAudios()
{
    var audio_loaded = [];
    document.querySelectorAll('audio').forEach(function(item)
    {
        let p = new Promise(function(ok, error)
        {
            item.addEventListener('canplaythrough', (e) => {
                ok();
            });
        });
        audio_loaded.push(p);
    });
    promisesAudio = Promise.all(audio_loaded).then(function()
    {
        loading = false;
        document.querySelector('.audio_player-container').classList.remove('loading');

    });
    return promisesAudio;
}

document.addEventListener('DOMContentLoaded', () => {
    var link = document.querySelector('.credito_iframe a');
    if(link)
    {
        link.addEventListener('click', (e) => {
            e.stopPropagation();
            e.preventDefault();
            link.innerHTML='<iframe src="'+link.getAttribute('href')+'" />';
        });
    }
    document.querySelectorAll('audio').forEach( (x) => x.load() );

    audios[0].addEventListener('loadedmetadata', (e) => {
        document.querySelector('.audio_total').innerText = toDuration(audios[0].duration);
        document.querySelector('.audio_total2').innerText = toDuration(audios[0].duration);
    });

    loadAudios();
    // debug here
    document.querySelector('.audio_player-container').classList.remove('loading');
    loading = false;

    document.querySelector('.audio_play').addEventListener('click', (e) =>
        {
            if(loading)
            {
                return false;
            }
            let was_paused =  paused;
            let play=e.target;
            audios.forEach((audio) => {
                if(!was_paused) { paused = true; audio.pause(); }
                else { paused = false; audio.play(); }
            });
            updatePlayState();
        });
    document.querySelector('.audio_back').addEventListener('click', (e) =>
        {
            let play=e.target;
            let target = Math.max(0, audios[0].currentTime - 5);
            audios.forEach((audio) => {
                audio.currentTime = target;
            });
            updatePlayState();
        });
    document.querySelector('.audio_next').addEventListener('click', (e) =>
        {
            let play=e.target;
            let target = Math.min(audios[0].duration, audios[0].currentTime + 5);
            audios.forEach((audio) => {
                audio.currentTime = target;
            });
            updatePlayState();
        });
    document.querySelector('.audio_restart').addEventListener('click', (e) =>
        {
            if(loading)
            {
                return false;
            }
            let play=e.target;
            audios.forEach((audio) => {
                audio.currentTime = 0;
            });
            audios.forEach((audio) => audio.play());
            updatePlayState();
        });

    // Speed range handle
    document.querySelector('.audio_speed_minus').addEventListener('click', () => { 
        speed -= 10; updateSpeed() 
    });
    document.querySelector('.audio_speed_more').addEventListener('click', () => { 
        speed += 10 ; updateSpeed()
    });

    function updatePlayState()
    {
        let play_image = document.querySelector('.audio_play img');
        play_image.setAttribute('src', images_path+ (paused ? 'play' : 'pause')+'.png');
    }
    function updateSpeed()
    {
        document.querySelector('.current_speed').innerText = '%'+speed;
        audios.forEach((audio) => 
            {
                audio.playbackRate = speed/100;
            });
    }

    // Set default volume
    audios.forEach((audio) => {
        audio.volume = parseInt(audio.getAttribute('data-volume'),10) / 100;
        audio.wantedvolume = 1;
    });

    document.querySelectorAll('.audio_mute input').forEach((input) =>
        {
            input.addEventListener('change' , () => {
                var button = input.parentElement.querySelector('button');
                let audio = document.querySelector('#'+button.getAttribute('data-ref'));

                audio.volume = input.value * parseInt(audio.getAttribute('data-volume'),10) / 100;
                switch(input.value)
                {
                    case "0" : button.classList.add('muted'); break;
                    case "1" : button.classList.remove('muted'); break;
                    case "0.9" : button.classList.remove('muted'); break;
                    default : button.classList.remove('muted'); break;
                }
            });

        });
    document.querySelectorAll('.audio_mute button').forEach((button) =>
        {
            button.addEventListener('click' , () => {
                var range = button.parentElement.querySelector('input');
                range.value = range.value==="0" ? 1 : 0;
                range.dispatchEvent(new Event('change'));
            });
        });

    audios[0].addEventListener('timeupdate', progressUpdate.bind(this));

    document.querySelector('.audio_percent').addEventListener('click', function(e)
        {
            if(loading)
            {
                return false;
            }
            var x = (e.clientX - e.target.getBoundingClientRect().x) / e.target.offsetWidth;
            audios.forEach((audio) => audio.pause());

            audios.forEach((audio) => {
                audio.currentTime = audios[0].duration * x;
            });
            paused=false;
            audios.forEach((audio) => audio.play());
            updatePlayState();
        });

    document.querySelector('.audio_current').innerText = toDuration(0);
    document.querySelector('.audio_current2').innerText = toDuration(0);



    function toDuration(sec)
    {
        let min = 0;
        while(sec>=60)
        {
            min++;
            sec-=60;
        }
        return min+':'+(sec<10 ? '0'+parseInt(sec): parseInt(sec));
    }

    function progressUpdate(e)
    {
        document.querySelector('.audio_current').innerText = toDuration(audios[0].currentTime);
        document.querySelector('.audio_percent').style.setProperty('--percent', (audios[0].currentTime / audios[0].duration * 100)+'%');
    }

    // Avoid audio desync
    var audio_sync_timer;
    function sync_audio()
    {
        audio_sync_timer = window.setInterval(function()
        {
            if(paused) { return true; }
            let audiosMinusFirst = Array.from(audios);
            audiosMinusFirst.shift();

            let needSync = false;
            audiosMinusFirst.forEach((audio) => {
                if(audio.volume && Math.abs(audio.currentTime - audios[0].currentTime) >  0.06)
                {
                    needSync = true;
                }
            });
            if(needSync)
            {
                audios.forEach((x) => 
                {
                    x.pause();
                });
                audios.forEach((x) => 
                {
                    x.currentTime = audios[0].currentTime;
                });
                window.setTimeout(function()
                {
                    audios.forEach((x) => x.play());
                }, 200);
            }
        }, 1000);
    }
    sync_audio();



    function changeAudio(action,e)
    {
        if(action=='play')
        {
            audios.forEach((audio) => {
                if(audio.currentTime != e.target.currentTime) { audio.currentTime = e.target.currentTime; }
            });
            audios.forEach((audio) => audio.play());
        }
        else if(action=='pause')
        {
            audios.forEach((audio) => { audio.pause() });
        }
    }

    document.querySelectorAll('a.embed').forEach((link) => 
        {
            link.addEventListener('click', function(e)
                {
                    let link = e.target;
                    let href  = link.href;
                    if(href.match(/youtube.com\/embed/))
                    {
                        href += '?autoplay=1';
                    }
                    openPopin(href);
                    e.preventDefault();
                    return false;
                }, false);
        });

    function openPopin(url)
    {
        var div = document.createElement('div');
        div.classList.add('popin');
        document.body.appendChild(div);

        div.innerHTML= `
        <div class="iframe_container">
            <div class="iframe">
            <button class="popin_close" aria-label="Close">X</button>
            <iframe src="${url}"></iframe>
            </div>
        </div>
    `;

        document.querySelector('.popin').addEventListener('click', function()
            {
                document.body.removeChild(document.querySelector('.popin'));
            });

    }

});;
