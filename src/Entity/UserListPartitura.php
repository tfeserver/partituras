<?php

namespace App\Entity;

use App\Repository\UserListPartituraRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserListPartituraRepository::class)
 */
class UserListPartitura
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=UserList::class, inversedBy="userListPartituras")
     * @ORM\JoinColumn(nullable=false)
     */
    private $UserList;

    /**
     * @ORM\ManyToOne(targetEntity=Partitura::class, inversedBy="userListPartituras")
     */
    private $Partitura;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ordernumber;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserList(): ?UserList
    {
        return $this->UserList;
    }

    public function setUserList(?UserList $UserList): self
    {
        $this->UserList = $UserList;

        return $this;
    }

    public function getPartitura(): ?Partitura
    {
        return $this->Partitura;
    }

    public function setPartitura(?Partitura $Partitura): self
    {
        $this->Partitura = $Partitura;

        return $this;
    }

    public function getOrdernumber(): ?int
    {
        return $this->ordernumber;
    }

    public function setOrdernumber(?int $ordernumber): self
    {
        $this->ordernumber = $ordernumber;

        return $this;
    }
}
