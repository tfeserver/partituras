<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserFavRepository")
 */
class UserFav
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userFavs")
     */
    private $User;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Partitura", inversedBy="userFavs")
     */
    private $Partitura;

    /**
     * @ORM\ManyToOne(targetEntity=UserList::class, inversedBy="favs")
     */
    private $userList;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getPartitura(): ?Partitura
    {
        return $this->Partitura;
    }

    public function setPartitura(?Partitura $Partitura): self
    {
        $this->Partitura = $Partitura;

        return $this;
    }

    public function getUserList(): ?UserList
    {
        return $this->userList;
    }

    public function setUserList(?UserList $userList): self
    {
        $this->userList = $userList;

        return $this;
    }
}
