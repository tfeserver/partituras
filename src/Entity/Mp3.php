<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Mp3Repository")
 * @Vich\Uploadable
 */
class Mp3
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Partitura", inversedBy="mp3s")
     * @ORM\JoinColumn(nullable=false)
     */
    private $partitura;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @Vich\UploadableField(mapping="mp3", fileNameProperty="mp3")
     * @Assert\File(mimeTypes={"audio/mpeg"})
     * @Assert\File(maxSize="10M")
     * @var File
     */
    private $mp3File;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $mp3;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Instrumento")
     */
    private $instrumento;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPartitura(): ?Partitura
    {
        return $this->partitura;
    }

    public function setPartitura(?Partitura $partitura): self
    {
        $this->partitura = $partitura;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMp3File(): ?File
    {
        return $this->mp3File;
    }
    public function getMp3(): ?string
    {
        return $this->mp3;
    }

    /**
     * @param File|UploadedFile $mp3File
     */
    public function setMp3File(?File $mp3File = null)
    {
        $this->mp3File = $mp3File;

        if (null !== $mp3File) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function setMp3($mp3): self
    {
        $this->mp3 = $mp3;

        echo "SET $mp3<br>\n";
        return $this;
    }

    public function getInstrumento(): ?Instrumento
    {
        return $this->instrumento;
    }

    public function setInstrumento(?Instrumento $instrumento): self
    {
        $this->instrumento = $instrumento;

        return $this;
    }

}
