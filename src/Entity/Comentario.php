<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ComentarioRepository")
 */
class Comentario
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Partitura", inversedBy="comentarios")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Partitura;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\Column(type="text")
     */
    private $Text;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Validated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPartitura(): ?Partitura
    {
        return $this->Partitura;
    }

    public function setPartitura(?Partitura $Partitura): self
    {
        $this->Partitura = $Partitura;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->Text;
    }

    public function setText(string $Text): self
    {
        $this->Text = $Text;

        return $this;
    }

    public function getValidated(): ?bool
    {
        return $this->Validated;
    }

    public function setValidated(bool $Validated): self
    {
        $this->Validated = $Validated;

        return $this;
    }
}
