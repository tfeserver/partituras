<?php

namespace App\Controller;

use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\UserList;
use App\Entity\UserListPartitura;
use App\Entity\Partitura;
use App\Entity\Filter;
use App\Form\UserListType;
use App\Form\FilterTypeSimple;
use App\Repository\UserListRepository;
use App\Repository\PartituraRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/")
 */
class UserListController extends AbstractController
{
    private $security;
    private $logged_user;

    public function __construct(Security $security)
    {
        $this->security = $security;

    }

    /**
     * @Route({ "eus" : "/zerrendak", "es":"/listas", "fr":"/listes"}, name="app_user_list_public", methods={"GET"})
     */
    public function index(UserListRepository $userListRepository): Response
    {
        $user = $this->security->getUser();
        $lists = $userListRepository->findBy(['public' => true]);

        return $this->render('user_list/index.html.twig', [
            'user' => $user,
            'user_lists' => $lists
        ]);
    }

    /**
     * @Route({ "eus" : "/nire_zerrendak", "es":"/mis_listas", "fr":"/mes_listes"}, name="app_user_list_private", methods={"GET"})
     */
    public function priv(UserListRepository $userListRepository): Response
    {
        $user = $this->security->getUser();
        if(!$this->security->isGranted('ROLE_ADMIN'))
        {
            $lists = $userListRepository->getVisibleLists($user);
        }
        else
        {
            $lists = $userListRepository->findAll();
        }

        return $this->render('user_list/index.html.twig', [
            'user' => $user,
            'user_lists' => $lists
        ]);
    }


    /**
     * @Route({ "eus": "/gogoenak", "es":"favoritos", "fr":"favoris"}, name="app_user_list_liked", methods={"GET"})
     */
    public function liked(PartituraRepository $partituraRepository, Request $request): Response
    {
        $this->getUserData($request);
        if(!$this->logged_user)
        {
            return $this->redirectToRoute('front_partitura', [], Response::HTTP_SEE_OTHER);
        }
        $partituras = $partituraRepository->findByUser($this->logged_user->getId());

        if($this->security->isGranted('ROLE_ADMIN'))
        {
            $partituras = $partituraRepository->findAll();
        }
        $user_favs = $this->security->getUser() ? $this->security->getUser()->getFavPartituras() : null;
        return $this->render('partitura/liked.html.twig', [
            'user_favs' => $user_favs,
            'partituras' => $partituras
        ]);
    }

    /**
     * @Route({ "es" : "/nueva_lista", "eus":"zerrenda_berria","fr":"nouvelle_liste"}, name="app_user_list_new", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER")
     */
    public function new(Request $request, UserListRepository $userListRepository): Response
    {
        $this->logged_user = $this->security->getUser() ? $this->security->getUser(): null;

        $userList = new UserList();
        $userList->setUser($this->logged_user);

        $form = $this->createForm(UserListType::class, $userList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userListRepository->add($userList);
            return $this->redirectToRoute('app_user_list_private', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('user_list/new.html.twig', [
            'user_list' => $userList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route({
     * "fr" : "/user{user}/{name}.fr",  "es" : "/user{user}/{name}.es",  "eus" : "/user{user}/{name}.eus" } , name="app_user_list_show", methods={"GET"})
     */
    public function show(Request $request, UserList $userList, $user, $name): Response
    {

        $user_favs = $this->security->getUser() ? $this->security->getUser()->getFavPartituras() : null;
        $get = $request->query->all();

        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $page = 1;
        $filter = new Filter();
        $filter->setCol('createdAt');
        $filter->setSort('desc');
        $filter_form = $this->createForm(FilterTypeSimple::class, $filter, [
             'action' => $this->generateUrl('app_user_list_show', ['user' => $user, 'name' => $name]),
             'method' => 'GET',
        ]);

        $filter_form->handleRequest($request);
        $num_per_pages = 50;
        $partituras = $repo->findToAdd($page, $num_per_pages, $filter);
        if(isset($get['json']))
        {
            $json = [
            ];
            foreach($partituras as $partitura)
            {
                $json[] = [
                    'id' => $partitura->getId(),
                    'add_link' => $this->generateUrl('app_user_list_add', ['list' => $userList->getId(), 'partitura' => $partitura->getId()]),
                    'mainPdf' => 'upload/musescore/'.$partitura->getMusescoreFile(),
                    'name' => $partitura->getName(),
                ];
            }
            $response = new Response();
            $response->setContent(json_encode([
                'partituras' => $json
            ]));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        $permission=true;
        if(!$this->security->isGranted('ROLE_ADMIN') && (!$this->logged_user || $userList->getUser()->getId() !== $this->logged_user->getId()))
        {
            $permission=false;
        }

        if(isset($get['download']))
        {
            $pdfs = [];
            $command = "cd /var/www/partituras/public/upload/musescore/ && convert -density 150 ";
            foreach($userList->getPartituras() as $partitura)
            {
                $command.= $partitura->getMusescoreFile()." ";
            }
            $command.= " userList".$userList->getId().".pdf";

            $env = array('HOME' => '/tmp/', 'PATH' => '/usr/local/bin:/usr/bin:/bin');

            $proc=proc_open("$command",
                array(
                    array("pipe","r"),
                    array("pipe","w"),
                    array("pipe","w")
                ),
                $pipes, __DIR__, $env);
            $msg = stream_get_contents($pipes[1]);
            $error = stream_get_contents($pipes[2]);
            $code = proc_close($proc);
            if($code!==0)
            {
                exit("Fail generating pdf: $error");
            }
            return $this->redirect('/upload/musescore/userList'.$userList->getId().".pdf");
        }
        return $this->render('user_list/show.html.twig', [
            'partituras' => $partituras,
            'filter_form' => $filter_form->createView(),
            'permission' => $permission,
            'user_favs' => $user_favs,
            'user_list' => $userList,
        ]);
    }


    /**
     * @Route({
      "es": "/order_to_list/{list}.es",
      "eus": "/order_to_list/{list}.eus",
      "fr": "/order_to_list/{list}.fr"
      }, * name="app_user_list_order", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function orderlist(Request $request, UserList $list)
    {
        $json = json_decode($request->getContent(),true);
        $ids = @$json['ids'];

        $entityManager = $this->getDoctrine()->getManager();
        foreach($list->getUserListPartituras() as $oldIdx => $userlistPartitura)
        {
            $id = $userlistPartitura->getId();
            $userlistPartitura->setOrdernumber(array_search($id, $ids));
            $entityManager->persist($userlistPartitura);
        }
        $entityManager->flush();

        $response = new Response();
        $response->setContent(json_encode([
            'partituras' => $json
        ]));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route({
      "es": "/add_to_list/{partitura}/{list}.es",
      "eus": "/add_to_list/{partitura}/{list}.eus",
      "fr": "/add_to_list/{partitura}/{list}.fr"
      }, * name="app_user_list_add", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function addtolist(Request $request, $partitura, $list)
    {
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $repoList = $this->getDoctrine()->getRepository(UserList::class);
        $repoListPartitura = $this->getDoctrine()->getRepository(UserListPartitura::class);
        $partitura = $repo->findById($partitura);
        $list = $repoList->findById($list);

        if(!$partitura || !$list)
        {
            throw $this->createNotFoundException('Does not exist.');
        }
        $partitura = $partitura[0];
        $list = $list[0];

        if(!$this->security->isGranted('ROLE_ADMIN') && $list->getUser()->getId() !== $this->logged_user->getId())
        {
            throw $this->createNotFoundException('Permission denied.');
        }
        $listPartitura = $repoListPartitura->findBy(['Partitura' => $partitura, 'UserList' => $list]);

        $response = new Response();
        $entityManager = $this->getDoctrine()->getManager();
        $listPartitura = new UserListPartitura();
        $listPartitura->setPartitura($partitura);
        $listPartitura->setUserList($list);
        $listPartitura->setOrdernumber(count($list->getPartituras())+1);
        $entityManager->persist($listPartitura);
        $entityManager->flush();
        return $this->redirectToRoute('app_user_list_show', ['user' => $list->getUser()->getId(), 'name' => $list->getName()], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route({"es": "/remove_from_list/{id}.es", "eus": "/remove_from_list/{id}.eus", "fr", "/remove_from_list/{id}.fr"}, * name="app_user_list_remove", methods={"GET","POST"})
     * @IsGranted("ROLE_USER")
     */
    public function removeTolist(Request $request, $id)
    {
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $repoListPartitura = $this->getDoctrine()->getRepository(UserListPartitura::class);
        $listPartitura = $repoListPartitura->findById($id);

        if(!$listPartitura)
        {
            throw $this->createNotFoundException('Does not exist.');
        }
        $list=$listPartitura[0]->getUserList();

        if(!$this->security->isGranted('ROLE_ADMIN') && $list->getUser()->getId() !== $this->logged_user->getId())
        {
            throw $this->createNotFoundException('Permission denied.');
        }
        $repoListPartitura->remove($listPartitura[0]);

        return $this->redirectToRoute('app_user_list_show', ['user' => $list->getUser()->getId(), 'name' => $list->getName()], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route({
     * "fr" : "/user{user}/{name}/{userlistpartituraId}.fr",  "es" : "/user{user}/{name}/{userlistpartituraId}.es",  "eus" : "/user{user}/{name}/{userlistpartituraId}.eus" } , name="app_user_list_view", methods={"GET"})
     */
    public function view(UserList  $userList ,UserListPartitura $userlistpartituraId): Response
    {
        $repo = $this->getDoctrine()->getRepository(Partitura::class);
        $user_favs = $this->security->getUser() ? $this->security->getUser()->getFavPartituras() : null;
        $partitura = $userlistpartituraId->getPartitura();

        $next_partitura = null;
        $previous_partitura = null;

        $prevTest = null;

        $idx=0;
        $total = count($userList->getUserListPartituras());

        foreach($userList->getUserListPartituras() as $temp_idx => $temp_userlistpartitura)
        {
            if($temp_userlistpartitura->getId() === $userlistpartituraId->getId())
            {
                $idx= $temp_idx+1;
                $previous_partitura = $prevTest;
            }
            if($prevTest && $prevTest->getId() === $userlistpartituraId->getId())
            {
                $next_partitura = $temp_userlistpartitura;
            }
            $prevTest = $temp_userlistpartitura;
        }

        return $this->render('user_list/view.html.twig', [
            'idx' => $idx,
            'total' => $total,
            'userList' => $userList,
            'partitura' => $partitura,
            'user_favs' => $user_favs,
            'next_partitura' => $next_partitura,
            'previous_partitura' => $previous_partitura,
            'user_list' => $userList,
        ]);
    }

    /**
     * @Route({"eus" : "zerrendak/{id}/edit", "es" : "/listas/{id}/edit" , "fr" : "/listes/{id}/edit"}, name="app_user_list_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, UserList $userList, UserListRepository $userListRepository): Response
    {
        $this->getUserData($request);
        if(!$this->security->isGranted('ROLE_ADMIN') && $userList->getUser()->getId() !== $this->logged_user->getId())
        {
            throw $this->createNotFoundException('Permission denied.');
        }
        $form = $this->createForm(UserListType::class, $userList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userListRepository->add($userList);
            return $this->redirectToRoute('app_user_list_private', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('user_list/edit.html.twig', [
            'user_list' => $userList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_user_list_delete", methods={"POST"})
     */
    public function delete(Request $request, UserList $userList, UserListRepository $userListRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$userList->getId(), $request->request->get('_token'))) {
            $userListRepository->remove($userList);
        }

        return $this->redirectToRoute('app_user_list_private', [], Response::HTTP_SEE_OTHER);
    }

    public function getUserData($request)
    {
        $this->logged_user = $this->security->getUser() ? $this->security->getUser()->getId() : null;
        if($this->logged_user)
        {
            $repo = $this->getDoctrine()->getRepository(User::class);
            $logged_users = $repo->findById($this->logged_user);
            $this->logged_user  =$logged_users[0];
        }
    }
}
