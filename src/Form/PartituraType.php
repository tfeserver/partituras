<?php

namespace App\Form;
use Acme\DemoBundle\Form\Type\FieldsetType;

use Doctrine\ORM\EntityRepository;
use App\Form\UserType as UserType;
use App\Form\PartituraPdfType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use App\Entity\Partitura;
use App\Entity\Tag;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\Regex as Regex;
use App\Repository\TagRepository as TagRepository;


class PartituraType extends AbstractType
{
    private $security;
    private $translator;
    public function __construct(Security $security, TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($this->security->isGranted('ROLE_ADMIN'))
        {
            $builder->add('visible');
            $builder->add('created_at', DateTimeType::class, ['widget' => 'single_text', 'format' => 'yyyy-MM-dd']);
        }
        $builder
            ->add('name', null, [
                'label' => 
                'Name of the Musicsheet', 
                'attr' => [
                    'placeholder' => 'Name of the Musicsheet',
                ],
                'required' => true
            ])
            ->add('text', null , [
                'label' => 'Subtitle of the Musicsheet', 
                'attr' => [
                    'placeholder' => 'Subtitle of the Musicsheet',
                ],
                'required' => false]);
        if($this->security->isGranted('ROLE_ADMIN'))
        {
            $builder->add('url', null , ['label' => 'URL customize', 'required' => false, 'required' => true, 'constraints' => [ new Regex('/^[\\wñ\\s\-\_\(\)]+$/') ]]);
        }
        $builder
            ->add('partiturapdfs', CollectionType::class ,
                [
                    'entry_type' => PartituraPdfType::class,  
                    'by_reference' => false, 
                    'allow_add' => true,
                    'allow_delete' => true,
                    'entry_options' => ['attr' => ['class' => 'sf_collection_item' ],  'creation' => $options['creation']],
                    'prototype' => true,
                    'row_attr' => [
                        'class' => 'pdfs'
                    ],
                    'attr' => [
                        'class' => 'multiple sf_collection'
                    ]
                ])
            ->add('mp3s', CollectionType::class ,
                [
                    'entry_type' => Mp3Type::class,  
                    'by_reference' => false, 
                    'allow_add' => true,
                    'allow_delete' => true,
                    'entry_options' => ['attr' => ['class' => 'sf_collection_item' ],  'creation' => $options['creation']],
                    'prototype' => true,
                    'attr' => [
                        'class' => 'multiple sf_collection'
                    ]
                ])
                ->add('musescoreFileFile', VichFileType::class, [
                    'label' => $this->translator->trans('Change music sheet'), 
                    'required' => $options['creation'], 
                    'attr' => ['accept' => '.pdf, .jpg, .jpeg, .png'], 
                    'allow_delete' => true ])
            ->add('authors', EntityType::class,
                [
                    'mapped' =>true,
                    'label' => $this->translator->trans('Authors'),
                    'class' => Tag::class,
                    'required' => false,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                    ->where('u.isAuthor = 1')
                                  ->orderBy('u.isAuthor, u.isType, u.name', 'ASC');
                    },
                    'row_attr' => [ 'class' => 'tags' ],
                    'choice_attr' => function($choice, $key, $value)
                    {
                        return ['data-type' => $choice->getType()];
                    },
                    'choice_label' => 'name'
                ])
            ->add('types', EntityType::class,
                [
                    'label' => 'Types',
                    'mapped' =>true,
                    'class' => Tag::class,
                    'required' => false,
                    'multiple' => true,
                    'row_attr' => [ 'class' => 'tags' ],
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                    ->where('u.isType = 1')
                                  ->orderBy('u.isAuthor, u.isType, u.name', 'ASC');
                    },
                    'choice_attr' => function($choice, $key, $value)
                    {
                        return ['data-type' => $choice->getType()];
                    },
                    'choice_label' => 'name'
                ])

            ->add('instruments', EntityType::class,
                [
                    'label' => 'Instruments',
                    'mapped' =>true,
                    'class' => Tag::class,
                    'required' => false,
                    'multiple' => true,
                    'row_attr' => [ 'class' => 'tags' ],
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                    ->where('u.isInstrument = 1')
                                  ->orderBy('u.isAuthor, u.isType, u.name', 'ASC');
                    },
                    'choice_attr' => function($choice, $key, $value)
                    {
                        return ['data-type' => $choice->getType()];
                    },
                    'choice_label' => 'name'
                ])

            ->add('origins', EntityType::class,
                [
                    'label' => 'Origins',
                    'mapped' =>true,
                    'class' => Tag::class,
                    'required' => false,
                    'multiple' => true,
                    'row_attr' => [ 'class' => 'tags' ],
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                    ->where('u.isOrigin = 1')
                                  ->orderBy('u.isAuthor, u.isType, u.name', 'ASC');
                    },
                    'choice_attr' => function($choice, $key, $value)
                    {
                        return ['data-type' => $choice->getType()];
                    },
                    'choice_label' => 'name'
                ])
            
            
            ->add('creditoUrl', null , ['row_attr' => ['class' => 'creditoUrl'], 'label' => 'Main Embedded iframe', 'required' => false])
            ->add('validationComment', TextareaType::class , ['row_attr' => ['class' => 'comment'], 'label' => 'Comentario', 'required' => false])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'creation' => false,
            'data_class' => Partitura::class,
        ]);
    }
}
