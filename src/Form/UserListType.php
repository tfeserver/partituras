<?php

namespace App\Form;

use App\Entity\UserList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserListType extends AbstractType
{
    private $security;
    private $translator;
    public function __construct(Security $security, TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('text')
            ->add('public', null, [
                //'help' => 'Check this field if you want the list to be visible by everyone.',
                'help_attr' => ['class' => 'help' ]
            ])
        ;
        if($this->security->isGranted('ROLE_ADMIN'))
        {
            //$builder->add('user');
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserList::class,
        ]);
    }
}
