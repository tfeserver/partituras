<?php

namespace App\EventListener;

use Vich\UploaderBundle\Event\Event;

class PdfListener
{
    public function onVichUploaderPostUpload(Event $event)
    {
        $object = $event->getObject();
        $mapping = $event->getMapping();

        $upload_path=null;
        if(method_exists($object,'getMusescoreFileFile'))
        {
            $upload_path = $object->getMusescoreFileFile()->getPathName();
        }
        if(method_exists($object,'getPdfFile'))
        {
            $upload_path = $object->getPdfFile()->getPathName();
        }

        if(!empty($upload_path))
        {
            $jpg = preg_replace('/\.[^\.]+$/','.jpg', $upload_path);
            $full = preg_replace('/\.[^\.]+$/','-full.jpg', $upload_path);

            $prefix = preg_replace("/.[^\.]+$/i","", $upload_path)."_thumb";
            $firstPage="";

            $commands=[];

            // First convert to PDF if needed
            if(!preg_match("/\.pdf/",$upload_path))
            {
                $pdfName = preg_replace("/\.[^\.]+$/",".pdf", $upload_path);
                $firstPage="[0]";
                $commands[] = "convert $upload_path -quality 100 $pdfName";
                $upload_path=$pdfName;
                if(method_exists($object,'getMusescoreFileFile'))
                {
                    $object->setMusescoreFile(basename($pdfName));
                }
                if(method_exists($object,'getPdfFile'))
                {
                    $object->setPdf(basename($pdfName));
                }
            }

            $commands[] = "convert -density 150 -background white ".$upload_path."[0] \"$jpg\"; convert -resize 800x800 -crop 800x443+0+0 $jpg $jpg; jpegoptim -m90 $jpg";
            //$commands[] = "convert -density 500 -background white ".$upload_path." -append \"${prefix}_%03d.jpg\"; montage -tile x1 -geometry 2000x ${prefix}_* ${prefix}.jpg && rm ${prefix}_*; jpegoptim ${prefix}.jpg";
            $env = array('HOME' => '/tmp/', 'PATH' => '/usr/local/bin:/usr/bin:/bin');

            foreach($commands as $command)
            {
                $proc=proc_open("$command",
                    array(
                        array("pipe","r"),
                        array("pipe","w"),
                        array("pipe","w")
                    ),
                    $pipes, __DIR__, $env);
                $msg = stream_get_contents($pipes[1]);
                $error = stream_get_contents($pipes[2]);
                $code = proc_close($proc);
                if($code!==0)
                {
                    exit("Ezin da fitxategia irakurtzea.");
                }
            }
        }
        // do your stuff with $object and/or $mapping...
    }

}
